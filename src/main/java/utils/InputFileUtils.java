package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputFileUtils {

    public static List<String> extraction(String filePath, String delimiter) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(filePath));
        scanner.useDelimiter(delimiter);
        List<String> groups = new ArrayList<>();
        while (scanner.hasNext()) {
            groups.add(scanner.next());
        }
        scanner.close();
        return groups;
    }

}
