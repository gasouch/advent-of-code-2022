package day8;

import utils.InputFileUtils;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 8 🚀");
        prepareData();
        System.out.println("Response for stage 1 must be : " + firstStage());
        System.out.println("Response for stage 2 must be : " + secondStage());
    }

    private static Long firstStage() {
        return trees.stream().filter(Main::isVisibleFromOutside).count();
    }

    private static Long secondStage() {
        return trees.stream().mapToLong(Main::calculateScenicScore).max().orElseThrow();
    }

    private static void prepareData() throws FileNotFoundException {
        List<String> input = InputFileUtils.extraction("day8/input.txt", "\n");
        for (int y = 0; y < input.size(); y++) {
            String line = input.get(y);
            for (int x = 0; x < line.length(); x++) {
                trees.add(new Tree(x, y, Character.getNumericValue(line.charAt(x))));
            }
        }
    }

    private static boolean isVisibleFromOutside(Tree tree) {
        List<Tree> line = trees.stream().filter(t -> t.y == tree.y).toList();
        List<Tree> column = trees.stream().filter(t -> t.x == tree.x).toList();
        IntPredicate tallerTreeFoundX = x -> {
            Tree cur = line.stream().filter(t -> t.y == tree.y && t.x == x).findFirst().orElseThrow();
            return cur.height >= tree.height;
        };
        IntPredicate tallerTreeFoundY = y -> {
            Tree cur = column.stream().filter(t -> t.x == tree.x && t.y == y).findFirst().orElseThrow();
            return cur.height >= tree.height;
        };
        boolean hiddenFromLeft = IntStream.range(0, tree.x).anyMatch(tallerTreeFoundX);
        boolean hiddenFromRight = IntStream.range(tree.x + 1, line.size()).anyMatch(tallerTreeFoundX);
        boolean hiddenFromTop = IntStream.range(0, tree.y).anyMatch(tallerTreeFoundY);
        boolean hiddenFromBottom = IntStream.range(tree.y + 1, column.size()).anyMatch(tallerTreeFoundY);
        return !(hiddenFromLeft && hiddenFromRight && hiddenFromTop && hiddenFromBottom);
    }

    private static Integer calculateScenicScore(Tree tree) {
        List<Tree> line = trees.stream().filter(t -> t.y == tree.y).toList();
        List<Tree> column = trees.stream().filter(t -> t.x == tree.x).toList();

        int nextL = line.stream().filter(t -> t.x < tree.x && t.height >= tree.height).mapToInt(t -> t.x).max().orElse(0);
        int nextR = line.stream().filter(t -> t.x > tree.x && t.height >= tree.height).mapToInt(t -> t.x).min().orElse(line.size() - 1);
        int nextT = column.stream().filter(t -> t.y < tree.y && t.height >= tree.height).mapToInt(t -> t.y).max().orElse(0);
        int nextB = column.stream().filter(t -> t.y > tree.y && t.height >= tree.height).mapToInt(t -> t.y).min().orElse(column.size() - 1);

        return (tree.x - nextL) * (nextR - tree.x) * (tree.y - nextT) * (nextB - tree.y);
    }

    private static class Tree {
        int x;
        int y;
        int height;

        Tree(int x, int y, int height) {
            this.x = x;
            this.y = y;
            this.height = height;
        }
    }

    private static final List<Tree> trees = new ArrayList<>();
}
