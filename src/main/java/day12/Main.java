package day12;

import utils.InputFileUtils;

import java.awt.*;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.List;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 12 🚀");
        createGraph();
        addPossibleMoves(start);
        System.out.println("Response for stage 1 must be : " + firstStage());
        System.out.println("Response for stage 2 must be : " + secondStage());
    }

    private static Integer firstStage() throws FileNotFoundException {
        return calculateShortestPathFrom(start);
    }

    private static Integer secondStage() throws FileNotFoundException {
        return graph.values().stream()
                .filter(node -> node.height == 'a')
                .mapToInt(Main::calculateShortestPathFrom)
                .min().orElseThrow();
    }

    private static Integer calculateShortestPathFrom(Node source) {
        source.distanceFromStart = 0;
        pQueue = new PriorityQueue<>();
        settled = new HashSet<>();
        pQueue.add(source);
        while (settled.size() < graph.size()) {
            if (pQueue.isEmpty()) {
                return goal.distanceFromStart;
            }
            Node current = pQueue.remove(); // Deleting the node that has the minimum distance from the priority queue
            if (settled.contains(current)) {
                continue;
            }
            settled.add(current);
            updateAdjacentNodesDistance(current);
        }
        return goal.distanceFromStart;
    }

    private static void updateAdjacentNodesDistance(Node current) {
        current.adjacentNodes.forEach(adjacent -> {
            // If the current node hasn't been already processed
            if (!settled.contains(adjacent)) {
                int newDist = current.distanceFromStart + 1;
                if (newDist < adjacent.distanceFromStart) {
                    adjacent.distanceFromStart = newDist;
                }
                pQueue.add(adjacent);
            }
        });
    }

    private static void createGraph() throws FileNotFoundException {
        graph = new HashMap<>();
        List<String> input = InputFileUtils.extraction("day12/input.txt", "\n");
        for (int y = 0; y < input.size(); y++) {
            String line = input.get(y);
            for (int x = 0; x < line.length(); x++) {
                Point coordinate = new Point(x, y);
                char c = line.charAt(x);
                switch (c) {
                    case 'S' -> {
                        start = new Node(coordinate, 'a');
                        graph.put(coordinate, start);
                    }
                    case 'E' -> {
                        goal = new Node(coordinate, 'z');
                        graph.put(coordinate, goal);
                    }
                    default -> {
                        Node node = new Node(coordinate, c);
                        graph.put(coordinate, node);
                    }
                }
            }
        }
    }

    private static void addPossibleMoves(Node from) {
        if (from.adjacentNodes.isEmpty()) { // Only if not already checked
            Arrays.asList(
                    new Point(from.coordinate.x - 1, from.coordinate.y),
                    new Point(from.coordinate.x + 1, from.coordinate.y),
                    new Point(from.coordinate.x, from.coordinate.y - 1),
                    new Point(from.coordinate.x, from.coordinate.y + 1)
            ).forEach(coordinate -> {
                if (graph.containsKey(coordinate) && graph.get(coordinate).height - from.height <= 1) {
                    from.adjacentNodes.add(graph.get(coordinate));
                    addPossibleMoves(graph.get(coordinate));
                }
            });
        }
    }

    private static class Node implements Comparable<Node> {
        Point coordinate;
        char height;
        int distanceFromStart = Integer.MAX_VALUE;
        List<Node> adjacentNodes = new ArrayList<>();

        Node(Point coordinate, char height) {
            this.coordinate = coordinate;
            this.height = height;
        }

        @Override
        public int compareTo(Node other) {
            return Integer.compare(distanceFromStart, other.distanceFromStart);
        }
    }

    private static Map<Point, Node> graph;
    private static PriorityQueue<Node> pQueue;
    private static Set<Node> settled;
    private static Node start;
    private static Node goal;
}
