package day17;

import utils.InputFileUtils;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 17 🚀");
        initPushes();
        System.out.println("Response for stage 1 (Real input) must be : " + firstStage()); // 3144 is false :-(
        //System.out.println("Response for stage 2 must be : " + secondStage());
    }

    private static Integer firstStage() {
        int round = 0;
        while (rocksLanded < 2022) {
            round++;
            push(pushes.get((round - 1) % pushes.size()));
            if (isRockLanded()) {
                rocksLanded++;
                System.out.println(rocksLanded + " rocks landed. Last one : " + curRock.shape);
                updateLevel();
                nextRock();
            } else {
                fall();
            }
            printRound(round);
        }
        return getLevelElevation();
    }

    private static void printRound(Integer round) {
        System.out.println("Round " + round);
        int elevation = getLevelElevation();
        IntStream.range(0, elevation + 1).forEach(line -> {
            StringBuilder sb = new StringBuilder("|");
            IntStream.range(0, 7).forEach(col -> {
                String value = level.get(col).get(elevation - line);
                sb.append(value == null ? "." : value);
            });
            sb.append("|");
            System.out.println(sb);
        });
        System.out.println("+-------+\n\n");
    }

    private static void updateLevel() {
        switch (curRock.shape) {
            case HORIZONTAL_BAR ->
                    IntStream.range(curRock.x, curRock.x + 4).forEach(x -> level.get(x).put(curRock.y, "#"));
            case PLUS -> {
                level.get(curRock.x).put(curRock.y + 1, "#");
                level.get(curRock.x + 1).put(curRock.y + 2, "#");
                level.get(curRock.x + 1).put(curRock.y + 1, "#");
                level.get(curRock.x + 1).put(curRock.y, "#");
                level.get(curRock.x + 2).put(curRock.y + 1, "#");
            }
            case INVERSED_L -> {
                level.get(curRock.x).put(curRock.y, "#");
                level.get(curRock.x + 1).put(curRock.y, "#");
                level.get(curRock.x + 2).put(curRock.y, "#");
                level.get(curRock.x + 2).put(curRock.y + 1, "#");
                level.get(curRock.x + 2).put(curRock.y + 2, "#");
            }
            case VERTICAL_BAR ->
                    IntStream.range(curRock.y, curRock.y + 4).forEach(y -> level.get(curRock.x).put(y, "#"));
            case CUBE -> {
                level.get(curRock.x).put(curRock.y, "#");
                level.get(curRock.x).put(curRock.y + 1, "#");
                level.get(curRock.x + 1).put(curRock.y, "#");
                level.get(curRock.x + 1).put(curRock.y + 1, "#");
            }
        }
    }

    private static boolean isRockLanded() {
        boolean landed = curRock.y == 0;
        if (!landed) {
            switch (curRock.shape) {
                case HORIZONTAL_BAR ->
                        landed = IntStream.range(curRock.x, curRock.x + 4).anyMatch(p -> level.get(p).get(curRock.y - 1) != null);
                case PLUS -> landed = level.get(curRock.x).get(curRock.y) != null
                        || level.get(curRock.x + 1).get(curRock.y - 1) != null
                        || level.get(curRock.x + 2).get(curRock.y) != null;
                case INVERSED_L ->
                        landed = IntStream.range(curRock.x, curRock.x + 3).anyMatch(p -> level.get(p).get(curRock.y - 1) != null);
                case VERTICAL_BAR -> landed = level.get(curRock.x).get(curRock.y - 1) != null;
                case CUBE ->
                        landed = IntStream.range(curRock.x, curRock.x + 2).anyMatch(p -> level.get(p).get(curRock.y - 1) != null);
            }
        }
        return landed;
    }


    private static Integer secondStage() {
        return 0;
    }

    private static void fall() {
        curRock.y--;
        System.out.println("Current rock " + curRock.shape + " fall one line. Current place=(" + curRock.x + "," + curRock.y + ")");
    }

    private static void push(Direction direction) {
        if (direction.equals(Direction.LEFT)) {
            boolean moveAuthorized = curRock.x > 0;
            if (moveAuthorized) {
                moveAuthorized = switch (curRock.shape) {
                    case HORIZONTAL_BAR -> level.get(curRock.x - 1).get(curRock.y) == null;
                    case PLUS -> level.get(curRock.x - 1).get(curRock.y + 1) == null
                            && level.get(curRock.x).get(curRock.y) == null
                            && level.get(curRock.x).get(curRock.y + 2) == null;
                    case INVERSED_L -> level.get(curRock.x - 1).get(curRock.y) == null
                            && level.get(curRock.x - 1).get(curRock.y + 1) == null
                            && level.get(curRock.x - 1).get(curRock.y + 2) == null;
                    case VERTICAL_BAR -> level.get(curRock.x - 1).get(curRock.y) == null
                            && level.get(curRock.x - 1).get(curRock.y + 1) == null
                            && level.get(curRock.x - 1).get(curRock.y + 2) == null
                            && level.get(curRock.x - 1).get(curRock.y + 3) == null;
                    case CUBE -> level.get(curRock.x - 1).get(curRock.y) == null
                            && level.get(curRock.x - 1).get(curRock.y + 1) == null;
                };
            }
            if (moveAuthorized) {
                curRock.x--;
                System.out.println("Current rock " + curRock.shape + " pushed left. Current place=(" + curRock.x + "," + curRock.y + ")");
            }
        } else {
            int shapeWidth = switch (curRock.shape) {
                case HORIZONTAL_BAR -> 4;
                case PLUS, INVERSED_L -> 3;
                case VERTICAL_BAR -> 1;
                case CUBE -> 2;
            };
            boolean moveAuthorized = curRock.x + shapeWidth < level.size();
            if (moveAuthorized) {
                moveAuthorized = switch (curRock.shape) {
                    case HORIZONTAL_BAR -> level.get(curRock.x + shapeWidth).get(curRock.y) == null;
                    case PLUS -> level.get(curRock.x + shapeWidth).get(curRock.y + 1) == null
                            && level.get(curRock.x + 2).get(curRock.y) == null
                            && level.get(curRock.x + 2).get(curRock.y + 2) == null;
                    case INVERSED_L -> level.get(curRock.x + shapeWidth).get(curRock.y) == null
                            && level.get(curRock.x + shapeWidth).get(curRock.y + 1) == null
                            && level.get(curRock.x + shapeWidth).get(curRock.y + 2) == null;
                    case VERTICAL_BAR -> level.get(curRock.x + shapeWidth).get(curRock.y) == null
                            && level.get(curRock.x + shapeWidth).get(curRock.y + 1) == null
                            && level.get(curRock.x + shapeWidth).get(curRock.y + 2) == null
                            && level.get(curRock.x + shapeWidth).get(curRock.y + 3) == null;
                    case CUBE -> level.get(curRock.x + shapeWidth).get(curRock.y) == null
                            && level.get(curRock.x + shapeWidth).get(curRock.y + 1) == null;
                };
            }
            if (moveAuthorized) {
                curRock.x++;
                System.out.println("Current rock " + curRock.shape + " pushed right. Current place=(" + curRock.x + "," + curRock.y + ")");
            }
        }
    }

    private static void nextRock() {
        Map.Entry<Integer, Shape> currentShape = shapes.entrySet().stream().filter(entry -> entry.getValue().equals(curRock.shape)).findFirst().orElseThrow();
        curRock = new Rock(shapes.get((currentShape.getKey() + 1) % 5), getLevelElevation() + 3, 2);
    }

    private static int getLevelElevation() {
        return level.values().stream()
                .mapToInt(map -> map.size() == 0 ? 0 : map.keySet().stream().mapToInt(k -> k).max().orElseThrow() + 1)
                .max().orElseThrow();
    }

    private static void initPushes() throws FileNotFoundException {
        List<String> input = InputFileUtils.extraction("day17/input.txt", "");
        input.forEach(c -> pushes.add("<".equals(c) ? Direction.LEFT : Direction.RIGHT));
    }

    private static class Rock {
        Shape shape;
        Integer y;
        Integer x;

        Rock(Shape shape, Integer y, Integer x) {
            this.shape = shape;
            this.y = y;
            this.x = x;
        }
    }

    private enum Shape {HORIZONTAL_BAR, PLUS, INVERSED_L, VERTICAL_BAR, CUBE}

    private enum Direction {LEFT, RIGHT}

    private static final Map<Integer, Shape> shapes = Map.of(0, Shape.HORIZONTAL_BAR, 1, Shape.PLUS, 2, Shape.INVERSED_L, 3, Shape.VERTICAL_BAR, 4, Shape.CUBE);
    private static final Map<Integer, Map<Integer, String>> level = Map.of(
            0, new HashMap<>(),
            1, new HashMap<>(),
            2, new HashMap<>(),
            3, new HashMap<>(),
            4, new HashMap<>(),
            5, new HashMap<>(),
            6, new HashMap<>()
    );
    private static final List<Direction> pushes = new ArrayList<>();
    private static Rock curRock = new Rock(shapes.get(0), 3, 2);
    private static int rocksLanded = 0;
}
