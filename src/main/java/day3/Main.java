package day3;

import utils.InputFileUtils;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Running Day 3 🚀");
        List<String> sacs = InputFileUtils.extraction("day3/input.txt", "\n");
        System.out.println("Response for stage 1 must be : " + firstStage(sacs));
        System.out.println("Response for stage 2 must be : " + secondStage(sacs));
    }

    final static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    private static int firstStage(List<String> sacs) {
        return sacs.stream()
                .mapToInt(sac -> {
                    String part1 = sac.substring(0, sac.length() / 2);
                    String part2 = sac.substring(sac.length() / 2);
                    char ano = part1.chars()
                            .mapToObj(i -> (char) i)
                            .filter(c -> part2.contains(String.valueOf(c)))
                            .findFirst()
                            .orElseThrow();
                    int positionInAlphabet = ALPHABET.indexOf(Character.toLowerCase(ano)) + 1;
                    return Character.isUpperCase(ano) ? positionInAlphabet + 26 : positionInAlphabet;
                })
                .sum();
    }

    private static int secondStage(List<String> sacs) {
        Map<Integer, List<String>> groups = new HashMap<>();
        for (int pos = 0; pos < sacs.size(); pos++) {
            int groupPos = pos / 3;
            if (groups.containsKey(groupPos)) {
                groups.get(groupPos).add(sacs.get(pos));
            } else {
                groups.put(groupPos, new ArrayList<>(Collections.singletonList(sacs.get(pos))));
            }
        }

        return groups.values().stream()
                .mapToInt(strings -> {
                    char common = strings.get(0).chars()
                            .mapToObj(i -> (char) i)
                            .filter(c -> strings.get(1).contains(String.valueOf(c)) && strings.get(2).contains(String.valueOf(c)))
                            .findFirst()
                            .orElseThrow();
                    int positionInAlphabet = ALPHABET.indexOf(Character.toLowerCase(common)) + 1;
                    return Character.isUpperCase(common) ? positionInAlphabet + 26 : positionInAlphabet;
                })
                .sum();
    }

}
