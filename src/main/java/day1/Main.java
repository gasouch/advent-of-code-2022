package day1;

import utils.InputFileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.ToIntFunction;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 1 🚀");
        List<String> elfes = InputFileUtils.extraction("day1/input.txt", "\n\n");
        System.out.println("Response for stage 1 must be : " + firstStage(elfes));
        System.out.println("Response for stage 1 must be : " + secondStage(elfes));
    }

    private static int firstStage(List<String> elfes) {
        return elfes.stream()
                .mapToInt(calculeCaloriesParElfe())
                .max()// On ne garde que celui qui a le plus de calories
                .orElseThrow();
    }

    private static int secondStage(List<String> elfes) {
        return elfes.stream()
                .mapToInt(calculeCaloriesParElfe())
                .map(calories -> -calories) // On inverse le nombre de calories
                .sorted() // On trie
                .map(calories -> -calories) // On remet à l'endroit 😁
                .limit(3) // On prend les 3 premiers
                .sum(); // On additionne
    }

    private static ToIntFunction<String> calculeCaloriesParElfe() {
        return elfe -> Arrays
                .stream(elfe.split("\n"))
                .mapToInt(Integer::parseInt)
                .sum();
    }
}
