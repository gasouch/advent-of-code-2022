package day5;

import utils.InputFileUtils;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

public class Main {
    private static Map<Integer, LinkedList<Character>> stacks;
    private static List<Move> moves;

    public static void main(String[] args) throws IOException {
        System.out.println("Running Day 5 🚀");
        initStacksAndMoves();
        System.out.println("Response for stage 1 must be : " + firstStage());
        initStacksAndMoves();
        System.out.println("Response for stage 2 must be : " + secondStage());
    }

    private static void initStacksAndMoves() throws FileNotFoundException {
        stacks = new HashMap<>(9);
        moves = new ArrayList<>();
        List<String> inputData = InputFileUtils.extraction("day5/input.txt", "\n\n");

        // Suppression de la ligne " 1   2   3   4   5   6   7   8   9"
        String[] stacksPart = inputData.get(0).split("\n");
        String[] stacks = Arrays.copyOf(stacksPart, stacksPart.length - 1);

        Arrays.stream(stacks)
                .forEach(line -> IntStream.of(1, 5, 9, 13, 17, 21, 25, 29, 33) // Parsing en mode colonné
                        .forEach(i -> {
                            // Padding pour sécuriser le parsing (la première ligne est moins large)
                            char c = String.format("%1$-" + 35 + "s", line).charAt(i);
                            if (c != ' ') {
                                addCharToStack(i, c);
                            }
                        })
                );


        String movesPart = inputData.get(1);
        moves.addAll(
                Arrays.stream(movesPart.split("\n"))
                        .map(line -> new Move(
                                        Integer.parseInt(line.split(" ")[1]),
                                        Integer.parseInt(line.split(" ")[3]),
                                        Integer.parseInt(line.split(" ")[5])
                                )
                        ).toList()
        );
    }

    private static void addCharToStack(int i, char c) {
        int stackId = switch (i) {
            case 1 -> 1;
            case 5 -> 2;
            case 9 -> 3;
            case 13 -> 4;
            case 17 -> 5;
            case 21 -> 6;
            case 25 -> 7;
            case 29 -> 8;
            case 33 -> 9;
            default -> throw new RuntimeException("pas d'bol mon pote...");
        };
        if (stacks.containsKey(stackId)) {
            stacks.get(stackId).addFirst(c);
        } else {
            stacks.put(stackId, new LinkedList<>(Collections.singletonList(c)));
        }
    }

    private static String firstStage() {
        moves.forEach(move ->
                IntStream.range(0, move.number)
                        .forEach(i -> stacks.get(move.to).addLast(stacks.get(move.from).removeLast()))
        );
        return stacks.values().stream()
                .map(stack -> Character.toString(stack.getLast()))
                .reduce("", String::concat);
    }

    private static String secondStage() {
        moves.forEach(move -> {
                    LinkedList<Character> listRemoved = new LinkedList<>();
                    IntStream.range(0, move.number)
                            .forEach(i -> listRemoved.add(stacks.get(move.from).removeLast()));
                    Collections.reverse(listRemoved);
                    stacks.get(move.to).addAll(listRemoved);
                }
        );
        return stacks.values().stream()
                .map(stack -> Character.toString(stack.getLast()))
                .reduce("", String::concat);
    }

    private static class Move {
        int number;
        int from;
        int to;

        Move(int number, int from, int to) {
            this.number = number;
            this.from = from;
            this.to = to;
        }
    }
}
