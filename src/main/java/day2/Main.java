package day2;

import utils.InputFileUtils;
import java.io.FileNotFoundException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 2 🚀");
        List<String> rounds = InputFileUtils.extraction("day2/input.txt", "\n");
        System.out.println("Response for stage 1 must be : " + firstStage(rounds));
        System.out.println("Response for stage 2 must be : " + secondStage(rounds));
    }

    private static int firstStage(List<String> rounds) {
        return rounds.stream()
                .mapToInt(round -> calculeScore(SonCoup.valueOf(round.split(" ")[0]), MonCoup.valueOf(round.split(" ")[1])))
                .sum();
    }

    private static int secondStage(List<String> rounds) {
        return rounds.stream()
                .mapToInt(round -> {
                    SonCoup sonCoup = SonCoup.valueOf(round.split(" ")[0]);
                    ResultatCible resultatCible = ResultatCible.valueOf(round.split(" ")[1]);
                    MonCoup monCoup = calculeMonCoup(sonCoup, resultatCible);
                    return calculeScore(sonCoup, monCoup);
                })
                .sum();
    }

    private static int calculeScore(SonCoup sonCoup, MonCoup monCoup) {
        int score = monCoup.score;
        if (sonCoup.perdContre == monCoup) { // Victoire = 6pts
            score += 6;
        } else if (sonCoup.nulContre == monCoup) { // Match nul = 3pts
            score += 3;
        }
        return score;
    }

    private static MonCoup calculeMonCoup(SonCoup sonCoup, ResultatCible resultatCible) {
        return switch (resultatCible) {
            case X -> sonCoup.gagneContre;
            case Y -> sonCoup.nulContre;
            case Z -> sonCoup.perdContre;
        };
    }

    enum SonCoup {
        A("Pierre", MonCoup.Z, MonCoup.Y, MonCoup.X),
        B("Feuille", MonCoup.X, MonCoup.Z, MonCoup.Y),
        C("Ciseau", MonCoup.Y, MonCoup.X, MonCoup.Z);
        final String libelle;
        final MonCoup gagneContre;
        final MonCoup perdContre;
        final MonCoup nulContre;

        SonCoup(String libelle, MonCoup gagneContre, MonCoup perdContre, MonCoup nulContre) {
            this.libelle = libelle;
            this.gagneContre = gagneContre;
            this.perdContre = perdContre;
            this.nulContre = nulContre;
        }

        @Override
        public String toString() {
            return libelle;
        }
    }

    enum ResultatCible {
        X("Défaite"),
        Y("Nul"),
        Z("Victoire");
        final String libelle;

        ResultatCible(String libelle) {
            this.libelle = libelle;
        }

        @Override
        public String toString() {
            return libelle;
        }
    }


    enum MonCoup {
        X("Pierre", 1),
        Y("Feuille", 2),
        Z("Ciseau", 3);
        final String libelle;
        final int score;
        MonCoup(String libelle, int score) {
            this.libelle = libelle;
            this.score = score;
        }

        @Override
        public String toString() {
            return libelle;
        }
    }

}
