package day10;

import utils.InputFileUtils;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 10 🚀");
        input = InputFileUtils.extraction("day10/input.txt", "\n");
        System.out.println("Response for stage 1 must be : " + firstStage());
        System.out.println("Display output for stage 2 is : ");
        secondStage();
    }

    private static Integer firstStage() {
        register = new HashMap<>(input.size());
        AtomicInteger cycle = new AtomicInteger();
        AtomicInteger x = new AtomicInteger(1);
        input.forEach(command -> {
            cycle.incrementAndGet();
            register.put(cycle.get(), x.get());
            if (command.startsWith("addx")) {
                cycle.incrementAndGet();
                register.put(cycle.get(), x.getAndAdd(Integer.parseInt(command.split(" ")[1])));
            }
        });
        return register.entrySet().stream()
                .filter(entry -> Arrays.asList(20, 60, 100, 140, 180, 220).contains(entry.getKey()))
                .mapToInt(entry -> Math.multiplyExact(entry.getKey(), entry.getValue()))
                .sum();
    }

    private static void secondStage() {
        register = new HashMap<>(input.size());
        crt = new HashMap<>(240);
        AtomicInteger cycle = new AtomicInteger();
        AtomicInteger x = new AtomicInteger(1);
        input.forEach(command -> {
            cycle.incrementAndGet();
            printCrt(cycle, x);
            if (command.startsWith("addx")) {
                cycle.incrementAndGet();
                printCrt(cycle, x);
                x.getAndAdd(Integer.parseInt(command.split(" ")[1]));
            }
        });
        crt.values().forEach(System.out::println);
    }

    private static void printCrt(AtomicInteger cycle, AtomicInteger x) {
        int crtRowIndex = 0;
        if (cycle.get() > 40 && cycle.get() <= 80) crtRowIndex = 1;
        else if (cycle.get() > 80 && cycle.get() <= 120) crtRowIndex = 2;
        else if (cycle.get() > 120 && cycle.get() <= 160) crtRowIndex = 3;
        else if (cycle.get() > 160 && cycle.get() <= 200) crtRowIndex = 4;
        else if (cycle.get() > 200 && cycle.get() <= 240) crtRowIndex = 5;
        if (!crt.containsKey(crtRowIndex)) crt.put(crtRowIndex, new StringBuilder());
        boolean isSpriteOk = x.get() - 1 <= crt.get(crtRowIndex).length() && x.get() + 1 >= crt.get(crtRowIndex).length();
        crt.get(crtRowIndex).append(isSpriteOk ? "#" : ".");
    }

    private static List<String> input;
    private static Map<Integer, Integer> register;
    private static Map<Integer, StringBuilder> crt;

}
