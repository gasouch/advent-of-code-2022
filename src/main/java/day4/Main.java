package day4;

import utils.InputFileUtils;

import java.io.IOException;
import java.util.*;

import static java.lang.Integer.parseInt;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Running Day 4 🚀");
        List<String> duos = InputFileUtils.extraction("day4/input.txt", "\n");
        System.out.println("Response for stage 1 must be : " + firstStage(duos));
        System.out.println("Response for stage 2 must be : " + secondStage(duos));
    }

    private static long firstStage(List<String> duos) {
        return duos.stream()
                .filter(duo -> {
                    String first = duo.split(",")[0];
                    String second = duo.split(",")[1];
                    int firstMin = parseInt(first.split("-")[0]);
                    int firstMax = parseInt(first.split("-")[1]);
                    int secondMin = parseInt(second.split("-")[0]);
                    int secondMax = parseInt(second.split("-")[1]);
                    return firstMin >= secondMin && firstMax <= secondMax || secondMin >= firstMin && secondMax <= firstMax;
                })
                .count();
    }

    private static long secondStage(List<String> duos) {
        return duos.stream()
                .filter(duo -> {
                    String first = duo.split(",")[0];
                    String second = duo.split(",")[1];
                    int firstMin = parseInt(first.split("-")[0]);
                    int firstMax = parseInt(first.split("-")[1]);
                    int secondMin = parseInt(second.split("-")[0]);
                    int secondMax = parseInt(second.split("-")[1]);
                    boolean firstHasAPartInSecond = firstMin >= secondMin && firstMin <= secondMax || firstMax >= secondMin && firstMax <= secondMax;
                    boolean secondHasAPartInFirst = secondMin >= firstMin && secondMin <= firstMax || secondMax >= firstMin && secondMax <= firstMax;
                    return firstHasAPartInSecond || secondHasAPartInFirst;
                })
                .count();
    }

}
