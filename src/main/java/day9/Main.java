package day9;

import utils.InputFileUtils;

import java.awt.*;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.List;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 9 🚀");
        List<String> moves = InputFileUtils.extraction("day9/input.txt", "\n");
        System.out.println("Response for stage 1 must be : " + firstStage(moves));
        System.out.println("Response for stage 2 must be : " + secondStage(moves));
    }

    private static Integer firstStage(List<String> moves) {
        Point header = new Point(0, 0);
        Point tail = new Point(0, 0);
        Set<Point> visitedPositions = new HashSet<>();
        moves.forEach(move -> {
            Direction dir = Direction.valueOf(move.split(" ")[0]);
            int steps = Integer.parseInt(move.split(" ")[1]);
            IntStream.range(0, steps)
                    .forEach(step -> {
                        moveHeader(header, dir);
                        moveKnot(tail, header, true, visitedPositions);
                    });
        });
        return visitedPositions.size();
    }

    private static Integer secondStage(List<String> moves) {
        List<Point> rope = List.of(new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0), new Point(0, 0));
        Set<Point> visitedPositions = new HashSet<>();
        moves.forEach(move -> {
            Direction dir = Direction.valueOf(move.split(" ")[0]);
            int steps = Integer.parseInt(move.split(" ")[1]);
            IntStream.range(0, steps)
                    .forEach(step -> {
                        moveHeader(rope.get(0), dir);
                        IntStream.range(1, rope.size()).forEach(i -> {
                            moveKnot(rope.get(i), rope.get(i - 1), i == rope.size() - 1, visitedPositions);
                        });
                    });
        });
        return visitedPositions.size() + 1;
    }

    private static void moveHeader(Point header, Direction dir) {
        switch (dir) {
            case L -> header.x = header.x - 1;
            case R -> header.x = header.x + 1;
            case U -> header.y = header.y + 1;
            case D -> header.y = header.y - 1;
        }
    }

    private static void moveKnot(Point knot, Point previous, boolean isTail, Set<Point> visitedPositions) {
        if (previous.distance(knot) >= 2) {
            if (previous.x > knot.x) {
                knot.x = knot.x + 1;
            }
            if (previous.x < knot.x) {
                knot.x = knot.x - 1;
            }
            if (previous.y > knot.y) {
                knot.y = knot.y + 1;
            }
            if (previous.y < knot.y) {
                knot.y = knot.y - 1;
            }
            if (isTail) {
                visitedPositions.add(new Point(knot));
            }
        }
    }

    private enum Direction {L, R, U, D}

}
