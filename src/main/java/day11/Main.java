package day11;

import utils.InputFileUtils;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Running Day 11 🚀");
        input = InputFileUtils.extraction("day11/input.txt", "\n\n");
        System.out.println("Response for stage 1 must be : " + firstStage());
        System.out.println("Response for stage 2 must be : " + secondStage());
    }

    private static Long firstStage() {
        initMonkeysForFirstRound();
        IntStream.range(0, 20).forEach(round -> computeRound());
        return monkeys.stream()
                .mapToLong(m -> -m.inspectionNumber)
                .sorted()
                .limit(2)
                .map(m -> -m)
                .reduce(1, (m1, m2) -> m1 * m2);
    }

    private static Long secondStage() {
        // Pas compris 😅
        return 0L;
    }

    private static void initMonkeysForFirstRound() {
        monkeys = input.stream()
                .map(monkeyToParse -> {
                    String[] parts = monkeyToParse.split("\n");
                    Function<Long, Long> operation = old -> {
                        List<String> operationParts = Arrays.stream(parts[2].split(" ")).toList();
                        Long operationFirstEl = operationParts.get(5).equals("old") ? old : Long.parseLong(operationParts.get(5));
                        Long operationSecondEl = operationParts.get(7).equals("old") ? old : Long.parseLong(operationParts.get(7));
                        if (operationParts.contains("+")) {
                            return operationFirstEl + operationSecondEl;
                        } else {
                            return operationFirstEl * operationSecondEl;
                        }
                    };
                    return new Monkey(
                            parts[0].split(":")[0].toLowerCase(),
                            Arrays.stream(parts[1].split(": ")[1].split(", "))
                                    .mapToLong(Long::parseLong)
                                    .boxed().collect(Collectors.toCollection(LinkedList::new)),
                            operation,
                            Long.parseLong(parts[3].split(" by ")[1]),
                            parts[4].split(" throw to ")[1],
                            parts[5].split(" throw to ")[1]
                    );
                })
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private static void computeRound() {
        monkeys.forEach(m -> {
            while (!m.startingItems.isEmpty()) {
                Long item = m.startingItems.removeFirst();
                Long worryValue = m.operation.apply(item);
                Long boredValue = Math.floorDiv(worryValue, 3);
                if (boredValue % m.divider == 0) {
                    getMonkey(m.monkeyTrue).startingItems.addLast(boredValue);
                } else {
                    getMonkey(m.monkeyFalse).startingItems.addLast(boredValue);
                }
                m.inspectionNumber++;
            }
        });
    }

    private static Monkey getMonkey(String id) {
        return monkeys.stream().filter(m -> m.id.equals(id)).findFirst().orElseThrow();
    }

    private static class Monkey {
        String id;
        LinkedList<Long> startingItems;
        //abstract Integer operation();
        Function<Long, Long> operation;
        Long divider;
        String monkeyTrue;
        String monkeyFalse;
        Long inspectionNumber = 0L;

        Monkey(String id, LinkedList<Long> startingItems, Function<Long, Long> operation, Long divider, String monkeyTrue, String monkeyFalse) {
            this.id = id;
            this.startingItems = startingItems;
            this.operation = operation;
            this.divider = divider;
            this.monkeyTrue = monkeyTrue;
            this.monkeyFalse = monkeyFalse;
        }
    }

    private static LinkedList<Monkey> monkeys;
    private static List<String> input;
}
