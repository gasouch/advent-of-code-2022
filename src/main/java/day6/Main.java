package day6;

import utils.InputFileUtils;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        List<Character> input = InputFileUtils.extraction("day6/input.txt", "\n").get(0).chars().mapToObj(i -> (char) i).toList();
        System.out.println("Running Day 6 🚀");
        System.out.println("Response for stage 1 must be : " + firstStage(input));
        System.out.println("Response for stage 2 must be : " + secondStage(input));
    }

    private static int firstStage(List<Character> input) {
        return findIndex(input, 4);
    }

    private static int secondStage(List<Character> input) {
        return findIndex(input, 14);
    }

    private static int findIndex(List<Character> input, int markerSize) {
        int markerIndex = IntStream.range(0, input.size())
                .filter(index -> index + markerSize < input.size() && isValidMarker(input.subList(index, index + markerSize), markerSize))
                .findFirst().orElseThrow();
        return markerIndex + markerSize;
    }

    private static boolean isValidMarker(List<Character> marker, int markerSize) {
        return marker.stream().distinct().toList().size() == markerSize;
    }

}
