package day7;

import utils.InputFileUtils;

import java.io.FileNotFoundException;
import java.util.*;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        List<String> cmdsWithOutput = InputFileUtils.extraction("day7/input.txt", "\\$ ");
        initArbo(cmdsWithOutput);
        System.out.println("Running Day 7 🚀");
        System.out.println("Response for stage 1 must be : " + firstStage());
        System.out.println("Response for stage 2 must be : " + secondStage());
    }

    private static Long firstStage() {
        root.addToEligibleForPart1IfNeeded();
        return eligibleDirsPart1.stream()
                .mapToLong(Directory::getTotalSize)
                .sum();
    }

    private static Long secondStage() {
        long available = 70000000L - root.getTotalSize();
        long spaceNeeded = 30000000L - available;
        return flatMapPart2.values().stream()
                .filter(dir -> dir.getTotalSize() >= spaceNeeded)
                .mapToLong(Directory::getTotalSize)
                .min().orElseThrow();
    }

    private static void initArbo(List<String> cmdsWithOutput) {
        cmdsWithOutput.forEach(cmdWithOutput -> {
            List<String> cmdWithOutputSplitted = Arrays.stream(cmdWithOutput.split("\n")).toList();
            String command = cmdWithOutputSplitted.get(0);
            if (command.startsWith("cd")) {
                executeCd(command.split(" ")[1]);
            } else if ("ls".equals(command)) {
                List<String> output = cmdWithOutputSplitted.subList(1, cmdWithOutputSplitted.size());
                executeLs(output);
            }
        });
    }

    private static void executeCd(String directory) {
        if ("..".equals(directory)) {
            currentLocation = currentLocation.substring(0, currentLocation.lastIndexOf("/"));
        } else if ("/".equals(directory)) {
            currentLocation = "/";
        } else {
            currentLocation += "/".equals(currentLocation) ? directory : "/" + directory;
        }
    }

    private static void executeLs(List<String> output) {
        Directory current = "/".equals(currentLocation) ? root : root.findSubDirInArbo(currentLocation).orElseThrow();
        current.totalSize = output.stream()
                .filter(line -> !"dir".equals(line.split(" ")[0]))
                .mapToLong(line -> Long.parseLong(line.split(" ")[0]))
                .sum();
        output.stream()
                .filter(line -> "dir".equals(line.split(" ")[0]))
                .map(line -> new Directory(("/".equals(currentLocation) ? currentLocation : currentLocation + "/") + line.split(" ")[1]))
                .forEach(dir -> current.subDirectories.add(dir));
        flatMapPart2.put(current.path, current);
    }

    private static class Directory {
        Long totalSize;
        String path;
        List<Directory> subDirectories = new ArrayList<>();

        Directory(String path) {
            this.path = path;
        }

        Long getTotalSize() {
            return totalSize + subDirectories.stream().mapToLong(Directory::getTotalSize).sum();
        }

        Optional<Directory> findSubDirInArbo(String location) {
            Optional<Directory> dir = subDirectories.stream()
                    .filter(subDir -> location.equals(subDir.path))
                    .findFirst();
            if (dir.isEmpty() && !subDirectories.isEmpty()) {
                dir = subDirectories.stream()
                        .filter(subDir -> subDir.findSubDirInArbo(location).isPresent())
                        .map(subDir -> subDir.findSubDirInArbo(location))
                        .findFirst().orElse(Optional.empty());
            }
            return dir;
        }

        void addToEligibleForPart1IfNeeded() {
            if (getTotalSize() < 100000L) {
                eligibleDirsPart1.add(this);
            }
            subDirectories.forEach(Directory::addToEligibleForPart1IfNeeded);
        }
    }

    static String currentLocation = "";
    static Directory root = new Directory("/");
    static List<Directory> eligibleDirsPart1 = new ArrayList<>();
    static Map<String, Directory> flatMapPart2 = new HashMap<>();
}
