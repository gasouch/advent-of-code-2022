# Advent Of Code 2022

Mes implem en `Java 18` de [l'Advent Of Code 2022](https://adventofcode.com/2022)

## Prérequis

Il faut compiler les classes utilitaires utilisées dans les exercices : 

```shell
javac src/main/java/utils/*
```

## Les exercices

Pour tester les exercices il faut se placer dans le répertoire `src/main/java` puis exécuter les commandes suivantes :

## Day 1

```shell
❯ java day1/Main.java
Running Day 1 🚀
Response for stage 1 must be : 71471
Response for stage 1 must be : 211189
```

## Day 2

```shell
❯ java day2/Main.java
Running Day 2 🚀
Response for stage 1 must be : 11475
Response for stage 2 must be : 16862
```

## Day 3

```shell
❯ java day3/Main.java
Running Day 3 🚀
Response for stage 1 must be : 7716
Response for stage 2 must be : 2973
```

## Day 4

```shell
❯ java day4/Main.java
Running Day 4 🚀
Response for stage 1 must be : 556
Response for stage 2 must be : 876
```

## Day 5

```shell
❯ java day5/Main.java
Running Day 5 🚀
Response for stage 1 must be : BZLVHBWQF
Response for stage 2 must be : TDGJQTZSL
```

## Day 6

```shell
❯ java day6/Main.java
Running Day 6 🚀
Response for stage 1 must be : 1816
Response for stage 2 must be : 2625
```

## Day 7

```shell
❯ java day7/Main.java
Running Day 7 🚀
Response for stage 1 must be : 1307902
Response for stage 2 must be : 7068748
```

## Day 8

```shell
❯ java day8/Main.java
Running Day 8 🚀
Response for stage 1 must be : 1859
Response for stage 2 must be : 332640
```

## Day 9

```shell
❯ java day9/Main.java
Running Day 9 🚀
Response for stage 1 must be : 6030
Response for stage 2 must be : 2545
```

## Day 10

```shell
❯ java day10/Main.java
Running Day 10 🚀
Response for stage 1 must be : 16060
Display output for stage 2 is :
###...##...##..####.#..#.#....#..#.####.
#..#.#..#.#..#.#....#.#..#....#..#.#....
###..#..#.#....###..##...#....####.###..
#..#.####.#....#....#.#..#....#..#.#....
#..#.#..#.#..#.#....#.#..#....#..#.#....
###..#..#..##..####.#..#.####.#..#.#....
```

## Day 11

```shell
❯ java day11/Main.java
Running Day 11 🚀
Response for stage 1 must be : 10605
Response for stage 2 must be : 0 // Pas compris 😅
```

## Day 12

```shell
❯ java day12/Main.java
Running Day 12 🚀
Response for stage 1 must be : 440
Response for stage 2 must be : 439
```